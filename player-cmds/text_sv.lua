-- Eventhandlers
RegisterCommand('text', function(source, args)
	textPlayer(source, args)
end)

RegisterCommand('txt', function(source, args)
	textPlayer(source, args)
end)

RegisterCommand('pm', function(source, args)
	textPlayer(source, args)
end)

function textPlayer(source, args)
	cm = stringsplit(convertTable(args), " ")
	
	local tPID = tonumber(cm[1])
	if GetPlayerName(tPID) ~= nil then
		local names2 = GetPlayerName(tPID)
		local names3 = GetPlayerName(source)
		local msgVar = {}
		local textmsg = ""
		for i=1, #cm do
			if i ~= 1 then
				textmsg = (textmsg .. " " .. tostring(cm[i]))
			end
		end		
		TriggerClientEvent('simp:textsent', source, tPID, names2)
		TriggerClientEvent('simp:textmsg', tPID, source, textmsg, names2, names3)
	else
		TriggerClientEvent('simp:error', source, tPID)
	end
end

RegisterCommand('reread', function(source, args)
	TriggerClientEvent('simp:recovermessage', source)
end)

--functions
function stringsplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

function convertTable(args)
	local msg = ""
	
	for k, v in pairs(args) do
		msg = msg .. " " .. v
	end
	
	return msg
end

function tablelength(T)
	local count = 0
	for _ in pairs(T) do count = count + 1 end
	return count
end
--end Functions