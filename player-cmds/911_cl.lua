RegisterCommand("911", function(source, args)
	if (args[1] ~= nil) then
		TriggerServerEvent('911Call', source, convertTable(args) .. " " .. "^1Location: ^3" .. getStreet(source))
	else
		TriggerEvent("chatMessage", "^7[^1911^7] ^8Please specify a message with your 911 call! ^7/911 <message>")
	end
end)

function getStreet(source)
	local street = ""
	local pos = GetEntityCoords(source)
	local var1, var2 = GetStreetNameAtCoord(pos.x, pos.y, pos.z, Citizen.ResultAsInteger(), Citizen.ResultAsInteger())
	local current_zone = GetLabelText(GetNameOfZone(pos.x, pos.y, pos.z))
	
	if GetStreetNameFromHashKey(var1) and GetNameOfZone(pos.x, pos.y, pos.z) then
		if GetStreetNameFromHashKey(var1) then
			if GetStreetNameFromHashKey(var2) == "" then
				street = current_zone
			else 
				street = GetStreetNameFromHashKey(var2) .. ", " .. GetLabelText(GetNameOfZone(pos.x, pos.y, pos.z))
			end
				street = GetStreetNameFromHashKey(var1)
		end
	end
	
	--street = GetStreetNameFromHashKey(var1)
	
	return street
end

function convertTable(args)
	local msg = ""
	
	for k, v in pairs(args) do
		msg = msg .. " " .. v
	end
	
	return msg
end