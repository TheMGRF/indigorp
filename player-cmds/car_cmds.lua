-- C O N F I G --
interactionDistance = 3.5 --The radius you have to be in to interact with the vehicle.

RegisterCommand('engine', function(source)
	local ped = GetPlayerPed(-1)
	
	local vehicle = GetVehiclePedIsIn(ped, false)
    if vehicle ~= nil and vehicle ~= 0 and GetPedInVehicleSeat(vehicle, 0) then
        SetVehicleEngineOn(vehicle, (not GetIsVehicleEngineRunning(vehicle)), false, true)
		showNotification("~g~Engine Toggled!")
    end
end, false)

RegisterCommand('trunk', function(source)
	local player = GetPlayerPed(-1)
	if controlsave_bool == true then
		vehicle = saveVehicle
	else
		vehicle = GetVehiclePedIsIn(player, true)
	end
			
	local isopen = GetVehicleDoorAngleRatio(vehicle,5)
	local distanceToVeh = GetDistanceBetweenCoords(GetEntityCoords(player), GetEntityCoords(vehicle), 1)
			
	if distanceToVeh <= interactionDistance then
		if (isopen == 0) then
			SetVehicleDoorOpen(vehicle,5,0,0)
		else
			SetVehicleDoorShut(vehicle,5,0)
		end
	else
		showNotification("~r~You must be near your vehicle to do that.")
	end
end, false)

RegisterCommand('rdoors', function(source)
	local player = GetPlayerPed(-1)
    if controlsave_bool == true then
		vehicle = saveVehicle
	else
		vehicle = GetVehiclePedIsIn(player,true)
	end
	local isopen = GetVehicleDoorAngleRatio(vehicle,2) and GetVehicleDoorAngleRatio(vehicle,3)
	local distanceToVeh = GetDistanceBetweenCoords(GetEntityCoords(player), GetEntityCoords(vehicle), 1)
			
	if distanceToVeh <= interactionDistance then
		if (isopen == 0) then
			SetVehicleDoorOpen(vehicle,2,0,0)
			SetVehicleDoorOpen(vehicle,3,0,0)
		else
			SetVehicleDoorShut(vehicle,2,0)
			SetVehicleDoorShut(vehicle,3,0)
		end
	else
		showNotification("~r~You must be near your vehicle to do that.")
	end
end, false)

RegisterCommand('hood', function(source)
	local player = GetPlayerPed(-1)
    if controlsave_bool == true then
		vehicle = saveVehicle
	else
		vehicle = GetVehiclePedIsIn(player,true)
	end
			
	local isopen = GetVehicleDoorAngleRatio(vehicle,4)
	local distanceToVeh = GetDistanceBetweenCoords(GetEntityCoords(player), GetEntityCoords(vehicle), 1)
			
	if distanceToVeh <= interactionDistance then
		if (isopen == 0) then
			SetVehicleDoorOpen(vehicle,4,0,0)
		else
			SetVehicleDoorShut(vehicle,4,0)
		end
	else
		showNotification("~r~You must be near your vehicle to do that.")
	end
end, false)

RegisterCommand('windows', function(source)
	local playerPed = GetPlayerPed(-1)
    if IsPedInAnyVehicle(playerPed, false) then
        local playerCar = GetVehiclePedIsIn(playerPed, false)
		if ( GetPedInVehicleSeat( playerCar, -1 ) == playerPed ) then 
            SetEntityAsMissionEntity( playerCar, true, true )
			if ( windowup ) then
				RollDownWindow(playerCar, 0)
				RollDownWindow(playerCar, 1)
				showNotification("~b~You rolled your windows down.")
				windowup = false
			else
				RollUpWindow(playerCar, 0)
				RollUpWindow(playerCar, 1)
				showNotification("~b~You rolled your windows up.")
				windowup = true
			end
		end
	end
end, false)

function showNotification( text )
    SetNotificationTextEntry( "STRING" )
    AddTextComponentString( text )
    DrawNotification( false, false )
end