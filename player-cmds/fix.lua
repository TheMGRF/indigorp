RegisterCommand('fix', function(source, args)
    fixCar(source, args)
end, false)

RegisterCommand('repair', function(source, args)
    fixCar(source, args)
end, false)

function fixCar(source, args)
	local playerPed = GetPlayerPed(-1)
	if IsPedInAnyVehicle(playerPed, false) then
		local vehicle = GetVehiclePedIsIn(playerPed, false)
		SetVehicleEngineHealth(vehicle, 1000)
		SetVehicleEngineOn( vehicle, true, true )
		SetVehicleFixed(vehicle)
		ShowNotification("~g~Your vehicle has been fixed!")
	else
		ShowNotification("~o~You're not in a vehicle! There is no vehicle to fix!")
	end
end

-- Shows a notification on the player's screen 
function ShowNotification( text )
    SetNotificationTextEntry( "STRING" )
    AddTextComponentString( text )
    DrawNotification( false, false )
end