RegisterCommand('coords', function()
	local ped = GetPlayerPed(-1)
	local pos = GetEntityCoords(ped, true)

	sendMessage(ped, "Pos X: " .. string.format("%.3f", pos.x) .. " Pos Y: " .. string.format("%.3f", pos.y) .. " Pos Z: " .. string.format("%.3f", pos.z))
end)

function sendMessage(player, msg)
    TriggerEvent("chatMessage", "", {255, 255, 255}, msg)
end