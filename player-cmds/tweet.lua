RegisterCommand("tweet", function(source, args)
	if (args[1] ~= nil) then
		if (args[2] ~= nil) then
			TriggerClientEvent('chatMessage', -1, "^7[^4Tweet^7] ^4" .. GetPlayerName(source) .. "^7: ^5" .. convertTable(args))
		end
	end
end)

function convertTable(args)
	local msg = ""
	
	for k, v in pairs(args) do
		msg = msg .. " " .. v
	end
	
	return msg
end