RegisterCommand("gan", function(source, args)
	if (args[1] ~= nil) then
		TriggerClientEvent('chatMessage', -1, "^8----------------------------------------")
		TriggerClientEvent('chatMessage', -1, "^1 GENERAL ANNOUNCEMENT")
		TriggerClientEvent('chatMessage', -1, convertTable(args))
		TriggerClientEvent('chatMessage', -1, "^8----------------------------------------")
	end
end)

function convertTable(args)
	local msg = ""
	
	for k, v in pairs(args) do
		msg = msg .. " " .. v
	end
	
	return msg
end