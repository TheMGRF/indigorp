RegisterNetEvent("simp:textsent")
AddEventHandler('simp:textsent', function(tPID, names2)
	SetNotificationTextEntry("STRING")
	AddTextComponentString()
	SetNotificationMessage("CHAR_DEFAULT", "CHAR_DEFAULT", true, 1, "Message to: " .. names2, "~g~Successfully Sent!")
	DrawNotification(false, true)
end)

RegisterNetEvent("simp:textmsg")
AddEventHandler('simp:textmsg', function(source, textmsg, names2, names3 )
	textData.hasRecievedMessage = true
	textData.lastPlayerMessage = textmsg
	textData.lastPlayermessageRecieved = source
	textData.lastMessagenames2 = names3
	SetNotificationTextEntry("STRING")
	AddTextComponentString(textmsg)
	SetNotificationMessage("CHAR_DEFAULT", "CHAR_DEFAULT", true, 1, "Message From: " .. names3, "")
	DrawNotification(false, true)
end)

RegisterNetEvent("simp:recovermessage")
AddEventHandler('simp:recovermessage', function()
	local textmsg = textData.lastPlayerMessage
	local ply = textData.lastPlayermessageRecieved
	local names3 = textData.lastMessagenames2
	SetNotificationTextEntry("STRING")
	AddTextComponentString(textmsg)
	SetNotificationMessage("CHAR_DEFAULT", "CHAR_DEFAULT", true, 1, "Message From: " .. names3, "")
	DrawNotification(false, true)
end)

RegisterNetEvent("simp:error")
AddEventHandler('simp:error', function(id)
	SetNotificationTextEntry("STRING")
	SetNotificationBackgroundColor(6)
	SetNotificationMessage("CHAR_DEFAULT", "CHAR_DEFAULT", true, 1, "Could Not Send Text", "Failed to find a player with ID: ~y~" .. id)
	DrawNotification(false, true)
end)