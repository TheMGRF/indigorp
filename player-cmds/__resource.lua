resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

ui_page 'ui/index.html'
files {
  'ui/index.html',
  'ui/style.css',
  'ui/img/cmds.png',
  'ui/img/help.png',
  'ui/img/rules.png',
  'ui/img/svrinfo.png',
  'ui/action.ogg',
  'ui/script.js',
}

client_script {
	'coords.lua',
	'car_cmds.lua',
	'car.lua',
	'fix.lua',
	'dv_cl.lua',
	'ui_cmds.lua',
	'911_cl.lua',
	'text_cl.lua',
	'text_vars.lua',
}

server_script {
	'ooc.lua',
	'911_sv.lua',
	'me.lua',
	'gan.lua',
	'tweet.lua',
	'admins.lua',
	'dispatch.lua',
	'dv_sv.lua',
	'text_sv.lua',
	'text_vars.lua',
}