AddEventHandler('chatMessage', function(source, name, msg)
	sm = stringsplit(msg, " ");
	if sm[1] == "/dispatch" then
		if (exports.discord_perms:IsRolePresent(source, "statepolice")) or (exports.discord_perms:IsRolePresent(source, "blainecounty")) or (exports.discord_perms:IsRolePresent(source, "lossantos")) or (exports.discord_perms:IsRolePresent(source, "highway")) then
			CancelEvent()
			TriggerClientEvent('chatMessage', -1, "^4^*Dispatch^7^r: " .. string.sub(msg,10))
		end
	end
end)

function stringsplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end