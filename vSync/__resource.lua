resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

server_export 'apiUpdateTime'
server_export 'apiUpdateWeather'

client_scripts {
    'vs_client.lua',
}

server_scripts {
    'vs_server.lua',
}
