-- CONFIG --

-- Allow passengers to shoot
local passengerDriveBy = true

-- CODE --

Citizen.CreateThread(function()
	while true do
		Wait(1)

		-- GetCurrentPedWeapon
		
		playerPed = GetPlayerPed(-1)
		car = GetVehiclePedIsIn(playerPed, false)
		if car then
			if GetSelectedPedWeapon(playerPed) == GetHashKey("weapon_unarmed") then
				SetPlayerCanDoDriveBy(PlayerId(), true)
			else
				if GetPedInVehicleSeat(car, -1) == playerPed then
					SetPlayerCanDoDriveBy(PlayerId(), false)
				elseif passengerDriveBy then
					SetPlayerCanDoDriveBy(PlayerId(), true)
				else
					SetPlayerCanDoDriveBy(PlayerId(), false)
				end
			end
		end
	end
end)